import random
import math

def CreateChanceCards():
    """Create all the Chance cards"""  

    pass

class Cards(object):
    """Simulate a deck of cards"""

    def __init__(self, new_pack):

        self.cards = []

        for i, card in enumerate(new_pack):
            card.id = i
            card.deck = self
            self.cards.append(card)
  
    # Creates a list containing the card indexes in a random order
    # Returns a list with 'nbrofcards' items in it
    def shuffle(self):
        
        nbr_of_cards = len(self.cards)        

        if nbr_of_cards > 1:
            cards = [None] * len(self.cards)
            random_nbr = random.randrange(0,nbr_of_cards)

            for card in self.cards:
                
                while cards[random_nbr] is not None:
                    random_nbr = random.randrange(0,nbr_of_cards)
                cards[random_nbr] = card

            self.cards = cards

    def pick_top_card(self):
        """Pops the top cards"""
        return self.cards.pop(0)

    def return_card(self, card, add_to_bottom = True):
        """Return a card that was removed from the pack"""
        self.cards.append(card)
        pass

class _Card(object):
    def __init__(self, description = None):
        self.id = None
        self.description = description
        self.deck = None

    def instruct(self, player):
        """To be implemented by sub classes"""
        pass

class _AdvanceToken(_Card):
    def __init__(self, destination_square, description = None):
        
        if description is None:
            description = "Advance To %s" % destination_square.description
       
        self.description = description
        self.destination_square = destination_square        
        return

    def instruct(self, player):
        """Move token"""
        self.player.move_token_to(self.destination_square, True)

class _CollectOrPayBanker(_Card):
    """Positive means collect from banker"""
    def __init__(self, banker, amount, description = None):

        if description is None:
            if amount > 0:
                description = "Collect ${} from bank".format(amount)
            else:
                description = "Pay banker ${}".format(-1 * amount)

        self.description = description
        self.amount = amount
        self.banker = banker
        return

    def instruct(self, player):
        """Collect or pay cash to the banker"""
        if self.amount > 0:
            self.banker.pay(self.player, self.amount)
        else:
            self.player.pay(self.banker, -1 * self.amount)

class _CollectOrPayPlayers(_Card):
    """Positive means collect from other players"""
    def __init__(self, players, amount, description = None):

        if description is None:
            if amount > 0:
                description = "Collect ${} from all players".format(amount)
            else:
                description = "Pay all players ${}".format(-1 * amount)

        self.description = description
        self.amount = amount
        self.players = players

        return

    def instruct(self, player):
        """Collect or pay all opponents"""
        for p in self.players:
            if p is not player:
                if self.amount > 0:
                    p.pay(player, self.amount)
                else:
                    player.pay(p, -1 * self.amount)

class GoBackThreeSpaces(_Card):
    def __init__(self):
        self.description = "Go Back 3 Spaces"
        return
    
    def instruct(self, player):
        player.move_token(-3, True)
        return        

class GoToJail(_Card):    
    def __init__(self):
        self.description = "Go To Jail"
        return
    
    def instruct(self, player):
        player.go_to_jail()

class _AdvanceToNearestGroup(_Card):
    def __init__(self, description, board, group_squares):
        self.description = description
        self.board = board
        self.group_squares = group_squares
        
    def instruct(self, player):
        """Move token to nearest station and notify the arrived by chance flag"""
        current = player.token.current_square
        # TODO - Logic to determine nearest square should
        # probably reside in the board class
        # as it would offer better re-use
        closest_distance = len(board.squares)
        closest_square = None
        for square in self.group_squares:
            distance = board.distance(current, square)
            if distance < closest_distance:
                closest_distance = distance 
                closest_square = square
               
        player.move_token_to(closest_square, True)

        return
 
class AdvanceToNearestRailroad(_AdvanceToNearestGroup):
    def __init__(self, board):
        self.description = "Advance Token To Nearest Railroad And Pay Owner Twice The Rent"
        super(self.__class__, self).__init__(self.description, board, board.stations)
   
class AdvanceToNearestUtility(_AdvanceToNearestGroup):    
    def __init__(self, board):
        self.description = """Advance token to nearest Utility. 
                              If unowned, you may buy it from the Bank. 
                              If owned, throw dice and pay owner a total 
                              ten times the amount thrown"""
        super(self.__class__, self).__init__(self.description, board, board.utilities)
 
class GetOutOfJailFree(_Card):
    def __init__(self):
        self.description = "Get Out Of Jail Free.  This Card May Be Kept Until Need Or Sold"
        
    def instruct(self, player):
        """Add the card to the player's collection of 
           get out of jail free cards"""
        assert self.deck is not None
        assert self in self.deck.cards
        player.get_out_of_jail_chance_cards.append(self)
       
class _MakePropertyImprovements(_Card):
    def __init__(self, house_cost, hotel_cost, properties, banker):
        self.description = ""
        self.house_cost = house_cost
        self.hotel_cost = hotel_cost
        self.properties = properties
        
    def instruct(self, player):
        # Get all properties owned by player
        charges = 0
        for prop in self.properties:
            if player is prop.owner:
                charges += self.house_cost * prop.houses
                charges += self.hotel_cost * prop.hotels

        if charges > 0:
            player.pay(banker, charges)

                
class BankPaysDividend(object):
    def __init__(self):
        self.description = "Bank Pays You Dividend Of $50"
        return
            
    def instruct(self, player, game):
        """Get 50 from Bank"""
        game.banker.pay(player, 50)
        return

class PayPoorTax(object):
    def __init__(self, banker):
        self.description = "Pay Poor Tax 15"
        self.banker = banker
        
    def instruct(self, player, game):
        """Pay bank 15"""
        player.pay(self.banker, 15)
        return

class AdvanceToPink1(object):
    def __init__(self, ):
        self.description = "Advance To St. Charles Pla"
        
    def instruct(self, player, game):
        player.move_token_to(player.board.square_pink1)
        return

class ElectedChairman(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        """Pay each player 15"""
        pass

class AdvanceToRailRoad1(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        pass

class AdvanceToDarkBlue2(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        pass

class BuildingLoanMatures(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        pass

class AdvanceToRed3(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        pass

class MakeGeneralRepairs(object):
    def __init__(self):
        self.description = ""
        
    def instruct(self, player, game):
        pass


