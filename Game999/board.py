class BoardSquare(object):
    """A square on the monopoly board"""

    def __init__(self, position, description, amount = None):
        if position < 0 or position > 39:
            raise ValueError('Position of square must be between 0 and 39')
        self.position = position 
        self.description = description
        self.amount = amount

    def instruct(self, game):
        """Instruct the player to do something when they
           land on the square"""
        print "Following instructions on {}".format(self.description)
        
        game.state = game.STATE_READY_TO_COMPLETE_TURN
        return

class Go(BoardSquare):
    """First square on the board which allows user to collect salary
       if the player lands on it or goes past."""
    def __init__(self):
        super(self.__class__, self).__init__(0, "GO")

class _DeedSquare(BoardSquare):
    """Board square where a player can earn rent when an opponent lands on it"""
    def __init__(self, position, deed):
        
        super(self.__class__, self).__init__(position, deed.name, deed.original_price)
        
        self.deed = deed
        
    def instruct(self, game):

        if self.deed.owner is None:
            # TODO - this should be done in deed init not here
            self.deed.owner = game.banker

        if self.deed.owner is game.banker:
            print "You must buy or auction this deed"
            game.state =  game.STATE_BUYING_DEED_FROM_BANKER
        elif self.deed.owner is not game.player_turning:
            print "You need to pay rent on this deed"
            self.deed.rent_due = True
            game.STATE_PENDING_RENT.start()
        else:
            game.autocomplete_turn()
            
    def deprecated_instruct(self, player, other_players, banker):

        deed = self.deed

        if banker.owns_deed(deed):
            # player can buy this or it goes for auction
            banker.offer_deed_for_sale(deed, player)
                
            if not player.owns_deed(deed):
                # player chose not to buy so now deed will be auctioned
                banker.offer_deed_for_auction(deed, other_players)

        return

Street = _DeedSquare
Utility = _DeedSquare
Railroad = _DeedSquare
        
class _RandomLuck(BoardSquare):
    """Square where you pick up a chance or community chest card"""
    def __init__(self, position, description):
        assert position in (2,7,17,22,33,36)
        super(self.__class__, self).__init__(position, description)
        
Community_chest = _RandomLuck
Chance = _RandomLuck

class Jail(BoardSquare):
    """Does nothing special or holds player for up to 3 turns"""
    def __init__(self):
        super(self.__class__, self).__init__(10, "Jail")

class FreeParking(BoardSquare):
    """Allows player to collect all fines paid out so far"""
    def __init__(self):
        super(self.__class__, self).__init__(20, "Free Parking")

class GoToJail(BoardSquare):
    """Sends player to jail without collecting salary"""
    def __init__(self):
        super(self.__class__, self).__init__(30, "Go To Jail")

class Tax(BoardSquare):
    """Sends player to jail without collecting salary"""
    def __init__(self, position, description, amount):
        if position in (4,38):
            super(self.__class__, self).__init__(position, description, amount)
        else:
            raise ValueError('The board position does not correspond to a Tax square')
        
class Board:
    """Simulates the board with all the squares"""
    
    def __init__(self, deeds):
        self.squares = []
        # Following are set up in init_squares - not sure if they will be useful
        self.railroad_squares = []
        self.utility_squares = [] 
        self.deed_squares = []
        self.non_deed_squares = []        

        self._init_squares(deeds)

    def _init_squares(self, deeds):
        """Create the board squares and add them to a list in the correct order"""

        # Order of creation matters as we add to the squares in that order

        self.square_go = Go()
        self.squares.append(self.square_go)
        
        self.square_brown1 = Street(len(self.squares), deeds.property_brown1)
        self.squares.append(self.square_brown1)

        self.square_community_chest_1 = Community_chest(len(self.squares), 'Community Chest')
        self.squares.append(self.square_community_chest_1)
        
        self.square_brown2 = Street(len(self.squares), deeds.property_brown2)
        self.squares.append(self.square_brown2)

        self.square_income_tax = Tax(len(self.squares), "Income Tax", 100)
        self.squares.append(self.square_income_tax)

        self.square_railroad1 = Railroad(len(self.squares), deeds.railroad1)
        self.squares.append(self.square_railroad1)
        
        self.square_light_blue1 = Street(len(self.squares), deeds.property_light_blue1)
        self.squares.append(self.square_light_blue1)

        self.square_chance_1 = Community_chest(len(self.squares), 'Chance')
        self.squares.append(self.square_chance_1)

        self.square_light_blue2 = Street(len(self.squares), deeds.property_light_blue2)
        self.squares.append(self.square_light_blue2)

        self.square_light_blue3 = Street(len(self.squares), deeds.property_light_blue3)
        self.squares.append(self.square_light_blue3)

        self.square_jail = Jail()
        self.squares.append(self.square_jail)

        self.square_pink1 = Street(len(self.squares), deeds.property_pink1)
        self.squares.append(self.square_pink1)

        self.square_electric_company = Utility(len(self.squares), deeds.utility1)
        self.squares.append(self.square_electric_company)
        
        self.square_pink2 = Street(len(self.squares), deeds.property_pink2)
        self.squares.append(self.square_pink2)

        self.square_pink3 = Street(len(self.squares), deeds.property_pink3)
        self.squares.append(self.square_pink3)

        self.square_railroad2 = Railroad(len(self.squares), deeds.railroad2)
        self.squares.append(self.square_railroad2)

        self.square_orange1 = Street(len(self.squares), deeds.property_orange1)
        self.squares.append(self.square_orange1)

        self.square_community_chest_2 = Community_chest(len(self.squares), 'Community Chest')
        self.squares.append(self.square_community_chest_2)

        self.square_orange2 = Street(len(self.squares), deeds.property_orange2)
        self.squares.append(self.square_orange2)

        self.square_orange3 = Street(len(self.squares), deeds.property_orange3)
        self.squares.append(self.square_orange3)

        self.square_free_parking = FreeParking()
        self.squares.append(self.square_free_parking)

        self.square_red1 = Street(len(self.squares), deeds.property_red1)
        self.squares.append(self.square_red1)

        self.square_chance_2 = Community_chest(len(self.squares), 'Chance')
        self.squares.append(self.square_chance_2)

        self.square_red2 = Street(len(self.squares), deeds.property_red2)
        self.squares.append(self.square_red2)

        self.square_red3 = Street(len(self.squares), deeds.property_red3)
        self.squares.append(self.square_red3)

        self.square_railroad3 = Railroad(len(self.squares), deeds.railroad3)
        self.squares.append(self.square_railroad3)

        self.square_yellow1 = Street(len(self.squares), deeds.property_yellow1)
        self.squares.append(self.square_yellow1)

        self.square_yellow2 = Street(len(self.squares), deeds.property_yellow2)
        self.squares.append(self.square_yellow2)

        self.square_water_works = Utility(len(self.squares), deeds.utility2)
        self.squares.append(self.square_water_works)

        self.square_yellow3 = Street(len(self.squares), deeds.property_yellow3)
        self.squares.append(self.square_yellow3)

        self.square_go_to_jail = GoToJail()
        self.squares.append(self.square_go_to_jail)

        self.square_green1 = Street(len(self.squares), deeds.property_green1)
        self.squares.append(self.square_green1)

        self.square_green2 = Street(len(self.squares), deeds.property_green2)
        self.squares.append(self.square_green2)

        self.square_community_chest_3 = Community_chest(len(self.squares), 'Community Chest')
        self.squares.append(self.square_community_chest_3)

        self.square_green3 = Street(len(self.squares), deeds.property_green3)
        self.squares.append(self.square_green3)

        self.square_railroad4 = Railroad(len(self.squares), deeds.railroad4)
        self.squares.append(self.square_railroad4)

        self.square_chance_3 = Community_chest(len(self.squares), 'Chance')
        self.squares.append(self.square_chance_3)

        self.square_dark_blue1 = Street(len(self.squares), deeds.property_dark_blue1)
        self.squares.append(self.square_dark_blue1)

        self.square_luxury_tax = Tax(len(self.squares), 'Super Tax', 75)
        self.squares.append(self.square_luxury_tax)

        self.square_dark_blue2 = Street(len(self.squares), deeds.property_dark_blue2)
        self.squares.append(self.square_dark_blue2)

        self.railroad_squares = [self.square_railroad1,
                          self.square_railroad2,
                          self.square_railroad3,
                          self.square_railroad4]

        self.utility_squares = [self.square_electric_company,
                          self.square_water_works]

        self.property_squares = [self.square_brown1, self.square_brown2,
                           self.square_light_blue1, self.square_light_blue2, self.square_light_blue3,
                           self.square_pink1, self.square_pink2, self.square_pink3,
                           self.square_orange1, self.square_orange2, self.square_orange3,
                           self.square_red1, self.square_red2, self.square_red3,
                           self.square_yellow1, self.square_yellow2, self.square_yellow3,
                           self.square_green1, self.square_green2, self.square_green3,
                           self.square_dark_blue1, self.square_dark_blue2]

        self.deed_squares.extend(self.property_squares)
        self.deed_squares.extend(self.railroad_squares)
        self.deed_squares.extend(self.utility_squares)

        for sq in self.squares:
            if sq not in self.deed_squares:
                self.non_deed_squares.append(sq)        

        return

    def distance(self, first_square, second_square):
        """Get number of squares to go from first_square to second_square"""
        second_position = second_square.position

        if second_position <= first_square.position:
            second_position += len(self.squares)

        return second_position - first_square.position
