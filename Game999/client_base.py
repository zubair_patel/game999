import pygame
import math
from PodSixNet.Connection import ConnectionListener, connection
from time import sleep

class ClientBase(ConnectionListener, object):
            
    # Network messages received from the server
    
    def Network_add_new_client(self, data):
        self._game.add_new_client(data["index"])
        return

    def Network_start_playing(self, data):
        self._game.start_playing()
        return

    def Network_complete_turn(self, data):
        self._game.complete_turn()
        return

    def Network_quit_game(self, data):
        self._game.quit_game()
        return
        
    def __init__(self, host, port, game):
        """This will connect to a running Server on the same
           host and port."""        
        self._game = game
        
        try:
            self.Connect((host, int(port)))
        except:
            print "Error Connecting to Server"
            print "Usage:", "host:port"
            print "e.g.", "localhost:31425"
            exit()

        return

    def update(self):
        """Pass messages back and forth between client and server"""
        connection.Pump()
        self.Pump()
        sleep(0.1)
        
    # Client messages sent to server for broadcasting

    def client_start_playing(self):
        """One-off event to begin the game for everyone"""
        self.Send({"action" : "start_playing"})
        return

    def client_quit_game(self):
        """Let all the players know to quit the game"""
        self.Send({"action" : "quit_game"})
        return
    
    def client_complete_turn(self):
        """Progress the turn to the next player"""
        self.Send({"action" : "complete_turn"})
        return

if __name__ == '__main__':
    host = "localhost"
    port = 8000
    c = ClientBase(host, port, None)
    c.client_start_playing()
    c.update()
    c.update()
    
