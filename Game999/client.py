import client_base

class Client(client_base.ClientBase):

    # Network messages received from the server
    
    def Network_dice_roll(self, data):
        die1 = data["die1"]   # value of dice
        die2 = data["die2"]
        return self._game.dice_roll(die1, die2)

    def Network_collect_salary(self, data):
        return self._game.collect_salary()

    def Network_demand_rent(self, data):
        index_player_demanding = data["index_player_demanding"]
        return self._game.demand_rent(index_player_demanding)

    def Network_buy_deed_just_landed(self, data):
        return self._game.buy_deed_just_landed()

    def __init__(self, host, port, game):
        return super(self.__class__, self).__init__(host, port, game)
    
    # Client messages sent to server for broadcasting

    def client_dice_roll(self):
        """Generate two random numbers from 1 to 6 to sim two dice being rolled"""
        import random

        assert(self._game.index_me == self._game.index_turn)
        die1 = random.randrange(1,7)
        die2 = random.randrange(1,7)

        self.Send({"action" : "dice_roll",
                   "die1" : die1,
                   "die2" : die2})
        return

    def client_collect_salary(self):
        """Collect salary when going past GO"""
        self.Send({"action" : "collect_salary"})
        return

    def client_demand_rent(self):
        """Demand rent from the current player"""
        self.Send({"action" : "demand_rent",
                   "index_player_demanding" : self._game.index_me})

    def client_buy_deed_just_landed(self):
        """Buy deed from banker that the player has just landed on"""
        self.Send({"action" : "buy_deed_just_landed"})

if __name__ == '__main__':
    host = "localhost"
    port = 8000
    c = Client(host, port, None)
    c.client_start_game()
    c.update()
    c.update()
    
