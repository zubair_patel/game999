import pygame
import state_base
import factory

def _pay_rent(player_demanding, player_turning):

    deed = None
    
    try:
        deed = player_turning.current_square.deed                
    except:
        # Current square is not a deed square so no rent to collect
        print "The current square is not a property"
        return

    if deed.owner is player_demanding:
        if deed.rent_due:
            print "Transfering rent"
            player_turning.account.transfer(deed.rent_amount(), player_demanding)
            deed.rent_due = False
        else:
            print "Oi oi, rent was already paid mister !"
    else:
        print "You don't own this property!"

    return

class _StateMonopolyBase(state_base.State):

    def move_token(self, die1, die2):
        """Do nothing. Override this method in base class if its meaningful
           to do so"""
        print "Can't move token"    
        return

    def execute_square_instruction(self):
        print "Can't execute square instruction"
        return

    def demand_rent(self, player_demanding):
        print "Can't demand rent"
        return

    def buy_deed(self, buyer, deed):
        print "You can't buy the deed"
        return

class StateClientsJoining(_StateMonopolyBase, state_base.StateClientsJoining):

    def start_playing(self):
        """Create the players, now that play has begun"""        

        state_base.StateClientsJoining.start_playing(self)

        self._game.gui = factory.create_gui(self._game)

        self._game.gui.set_caption(str(self._game.index_me + 1))

        self._game.state = self._game.STATE_WAITING_TO_ROLL_DICE
             
class StateWaitingToRollDice(_StateMonopolyBase, state_base.StateWaitingToCompleteTurn):

    def __init__(self, game):
        super(self.__class__, self).__init__(game)
  
    def move_token(self, die1, die2):
        """Dice was rolled by the current player so update the game state to reflect
        
           This includes :
           Checking current player's jail status
           Checking if doubles were thrown
           Progressing the player token
           Progressing the current player index to the next player
           Checking if player lands on a luck card, tax or jail"""
        
        current_player = self._game.player_turning

        # Salary can be collected if a player goes past GO which can be
        # determined if the new position is less than the original position
        orig_position = current_player.position
        current_player.can_collect_salary = False

        double_thrown = die1 == die2
        should_progress_turn = not double_thrown                

        if double_thrown:
            self._game.nbr_of_consecutive_doubles += 1
        else:
            self._game.nbr_of_consecutive_doubles = 0

        if double_thrown and current_player.is_in_jail():
            print "Player {} was in jail but released due to double thrown".format(self.index_turn + 1)
            current_player.get_out_of_jail()

        if double_thrown and self._game.nbr_of_consecutive_doubles == 3:
            print "Player {} threw 3 consecutive doubles so sent to jail".format(self.index_turn + 1)
            current_player.go_to_jail()
            should_progress_turn = True
            self._game.nbr_of_consecutive_doubles = 0

        # We are not going to set the next turn here but we are going to
        # set a flag that the turn will progress to the next player
        # Another state will actually do the progressing of the next player
        # index
        self._game.should_progress_turn = should_progress_turn

        if not should_progress_turn and double_thrown:
            print "Double was thrown so player {} must roll again".format(self._game.index_turn+1)
        
        if current_player.is_in_jail():
            self._game.state = self._game.STATE_READY_TO_COMPLETE_TURN            
        else:
            current_player.move(die1 + die2)
            print "Player {} rolled a {} and is now on {}".format(self._game.index_turn + 1, die1 + die2, current_player.current_square.description)

            if current_player.position < orig_position and not current_player.is_in_jail():
                current_player.can_collect_salary = True

            self._game.state = self._game.STATE_LANDING_ON_SQUARE

        return

class StateLandingOnSquare(_StateMonopolyBase):
    """Game goes into this state after moving token"""

    def execute_square_instruction(self):
        """Player can land on :
           GO
                Player can request his salary
           Visiting Jail
           Free Parking
           Go To Jail
           Random Luck
                Pick up card and follow more instructions
           Tax
           Deed
                Player either buys the deed
                Bank auctions the deed
                Other player can request payment"""
        
        print "Executing square instruction"
        
        game = self._game
        pt = game.player_turning
        sq = pt.current_square
        return sq.instruct(game)

    def demand_rent(self, player_demanding):
        return _pay_rent(player_demanding, self._game.player_turning)

class StateBuyingDeedFromBanker(_StateMonopolyBase):
    """Game goes into this state immediately after a player 
       lands on a square after rolling the dice.

       Depending on the square's compulsory instructions (ie
       where it doesn't depend on the player explicitly
       making a request"""

    def buy_deed(self, buyer, deed, price = None):
        deed.transfer_ownership(buyer, price)

    pass

class StateWaitingToCompleteTurn(_StateMonopolyBase, state_base.StateWaitingToCompleteTurn):

    def complete_turn(self):

        state_base.StateWaitingToCompleteTurn.complete_turn(self)

        self._game.player_turning.can_collect_salary = False

        self._game.state = self._game.STATE_WAITING_TO_ROLL_DICE
        
        return
    
class _StatePendingOperationBase(_StateMonopolyBase):
    def __init__(self, game, wait_seconds):
        #super(self.__class__, self).__init__(self, game)
        _StateMonopolyBase.__init__(self, game)
        
        self.wait_seconds = 5

        self.underlying_state = None
        self.start_ticks = None

        return
        
    def start(self, underlying_state = None):
        """Wrap the state and start the countdown"""

        self._wrap(underlying_state)            
        self.start_ticks = pygame.time.get_ticks()        
        print "Waiting for payment"

        return

    def _wrap(self, underlying_state = None):
        
        if not underlying_state:
            underlying_state = self._game.state
        
        self.underlying_state = underlying_state
        self._game.state = self

        return

    def stop(self):
        self._game.state = self.underlying_state
        self.underlying_state = None
        self.start_ticks = None
        print "Finished waiting"
        return

    def update(self):
        """Stay in this state for n seconds and then automatically
           move to the underlying state"""
                 
        if self.underlying_state:      
            elapsed_seconds = (pygame.time.get_ticks() - self.start_ticks) / 1000 # calc how many seconds
            if elapsed_seconds > self.wait_seconds: # if more than 10 seconds change state to underlying state
                self._game.state = self.underlying_state
                self.stop()

        return

    def complete_turn(self):
        print "You can't complete your turn as we are waiting"
        return

    def move_token(self, die1, die2):
        self.underlying_state.move_token(die1, die2)
        self._wrap()
        return 

    def execute_square_instruction(self):
        self.underlying_state.execute_square_instruction()
        self._wrap()
        return

class StatePendingRentPayment(_StatePendingOperationBase):
    def demand_rent(self, player_demanding):
        """Demand rent from the player turning if he has 
           landed on your property"""

        self.underlying_state.demand_rent(player_demanding)
        self.stop()
        return

# TODO
class StatePayingMoney(_StateMonopolyBase):
    """Game goes into this state when payment is demanded
       by either the banker (demand is automatic / compulsory)
       or by another player (when he remembers to ask)"""

    pass

class StateBuyingDeedFromPlayer(_StateMonopolyBase):
    """Game goes into this state immediately after a player 
       lands on a square after rolling the dice.

       Depending on the square's compulsory instructions (ie
       where it doesn't depend on the player explicitly
       making a request"""

    pass

class StateBuyingHouse(_StateMonopolyBase):
    """Game goes into this state immediately after  a player 
       lands ona square after rolling the dice.

       Depedning on the square's compulsory instructions (ie
       where it doesn't depend on the player explicitly
       making a request"""

    pass

class StateMortgagingDeed(_StateMonopolyBase):
    pass

class StateMortgagingHouse(_StateMonopolyBase):
    pass

class StateUnMortgagingDeed(_StateMonopolyBase):
    pass

class StateAuctionJailFreeCard(_StateMonopolyBase):
    pass

class StateAuctioningDeed(_StateMonopolyBase):
    pass



