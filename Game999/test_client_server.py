import unittest
import server
import client
import Queue
import threading
import time
import game
import pygame

class Test_server(unittest.TestCase):
    def test_connect_client_to_server(self):
        """Given a Server that is waiting for Clients to connect
           When I start up a new Client
           Then the Server will register me as a new Client
           And the Server will notify all Clients that I have connected"""

        host = "localhost"
        port = 8000

        def start_server(que):
            svr = server.Server(host, port)
            que.put(svr)
            svr.start_server()            

        que = Queue.Queue()
        thread = threading.Thread(target=start_server, args=(que,))
        thread.start()

        time.sleep(1)

        g = game.Game()        
        time.sleep(1)
        g._update()
       
        svr = que.get()

        try:
            svr.stop_server()
        except SystemExit:
            pass

        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()    
