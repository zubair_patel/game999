
class Account(object):
    """Cash account"""

    def __init__(self, notes):
        """Initialise bank notes"""
        allowed_denominations = 1,5,10,20,50,100,500
        assert all(denom in allowed_denominations for  denom in notes.keys())        
        
        self.notes = {500 : 0,
                      100 : 0,
                      50 : 0,
                      20 : 0,
                      10 : 0,
                      5 : 0,
                      1 : 0}

        for denom in notes:
            self.notes[denom] = notes[denom]

        return

    def balance(self):
        """Get total amount of cash in account"""

        return self._count_up_notes(self.notes)

    def _count_up_notes(self, notes):
        """Get total value of all the notes
           notes contains {Denom : NbrOfNotes}"""

        ret = 0

        if 500 in notes: ret = ret + notes[500] * 500
        if 100 in notes: ret = ret + notes[100] * 100
        if 50 in notes: ret = ret + notes[50] * 50
        if 20 in notes: ret = ret + notes[20] * 20
        if 10 in notes: ret = ret + notes[10] * 10
        if 5 in notes: ret = ret + notes[5] * 5
        if 1 in notes: ret = ret + notes[1] * 1

        return ret
    
    def transfer(self, amount, to_account):
        """Gets the correct note denominations, 
           breaking larger denominations to get the
           correct 'change' from the bank if necessary"""

        orig_from_balance = self.balance()
        assert orig_from_balance >= amount
        orig_to_balance = to_account.balance()        

        required_notes = self._get_notes_for_payment(amount)

        if(required_notes[100] > self.notes[100] or
           required_notes[50] > self.notes[50] or
           required_notes[20] > self.notes[20] or
           required_notes[10] > self.notes[10] or
           required_notes[5] > self.notes[5] or
           required_notes[1] > self.notes[1]):
            # Break the notes into smaller denoms
            self._break_notes()

        # Transfer the required notes to the to_account
        for note_value in required_notes:
            nbr_of_notes = required_notes[note_value]
            assert(self.notes[note_value] >= nbr_of_notes)
            self.notes[note_value] -= nbr_of_notes            
            to_account.notes[note_value] += nbr_of_notes

        assert self.balance() == orig_from_balance - amount
        assert to_account.balance() == orig_to_balance + amount

        return

    #def pay_banker(self, amount):
    #    """Banker never runs out"""
    #    # TODO - Get rid of this method and just use transfer_money()
    #    temp_banker = Account()
    #    self.transfer_money(amount, temp_banker)
    #    return

    #def collect_from_banker(self, amount):
    #    """Banker never runs out"""
    #    # TODO - Get rid of this method and just use transfer_money()
    #    temp_banker = Account(1000)
    #    temp_banker.transfer_money(amount, self)
    #    return

    def _get_notes_for_payment(self, amount):
        """Return list of notes required to make up the amount"""

        remaining = amount

        self._break_notes()

        required_500s = remaining / 500
        remaining = remaining % 500

        required_100s = remaining / 100
        remaining = remaining % 100
        
        required_50s = remaining / 50
        remaining = remaining % 50

        required_20s = remaining / 20
        remaining = remaining % 20

        required_10s = remaining / 10
        remaining = remaining % 10

        required_5s = remaining / 5
        remaining = remaining % 5

        required_1s = remaining / 1
        remaining = remaining % 1

        assert remaining is 0 
        
        ret = {1 : required_1s,
               5 : required_5s,
               10 : required_10s,
               20 : required_20s,
               50 : required_50s,
               100 : required_100s,
               500 : required_500s}

        assert self._count_up_notes(ret) == amount

        return ret
       
    def _break_notes(self):
        """Break the notes into smaller denominations"""

        orig_balance = self.balance()

        if self._need_change(100) and self.notes[500] > 0:
            self.notes[500] -= 1
            self.notes[100] += 5        
        if self._need_change(50) and self.notes[100] > 0:
            self.notes[100] -= 1
            self.notes[50] += 2
        if self._need_change(20) and self.notes[50] > 0:
            self.notes[50] -= 1
            self.notes[20] += 2
            self.notes[10] += 1
        if self._need_change(10) and self.notes[20] > 0:
            self.notes[20] -= 1
            self.notes[10] += 2
        if self._need_change(5) and self.notes[10] > 0:
            self.notes[10] -= 1
            self.notes[5] += 2
        if self._need_change(1) and self.notes[5] > 0:
            self.notes[5] -= 1
            self.notes[1] += 5

        assert(self.balance() == orig_balance)

    def _need_change(self, note_denomination):
        """Check if we need to break larger notes 
           for change"""

        assert note_denomination in (1,5,10,20,50,100)

        # min_notes_reqd -> {NoteDenom : MinRqd}
        min_notes_reqd = {1 : 5,
                          5 : 2,
                          10 : 3,
                          20 : 2,
                          50 : 2, 
                          100 : 5}

        return self.notes[note_denomination] < min_notes_reqd[note_denomination]
