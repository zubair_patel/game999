import PodSixNet.Channel
import PodSixNet.Server
from time import sleep


class ClientChannelBase(PodSixNet.Channel.Channel):

    def Network(self, data):
        """Send data to all connected clients.
           The 'data' arg is a dic containing the message details that we 
           want to send to all the clients"""
        print data
        self._server._notify_all_clients(data)        
        return        
    
    def Close(self):
        self._server.close()
        return


class ServerBase(PodSixNet.Server.Server, object):
 
    channelClass = ClientChannelBase    

    def __init__(self, host="localhost", port=8000):

        self._host = host
        self._port = port

        PodSixNet.Server.Server.__init__(self, localaddr=(host,port))

        self._client_channels = []

        self._running = False

    def start_server(self):
        print "STARTING SERVER ON {}".format(self._host)
        self._running = True
        while self._running is True:
            self.Pump()
            sleep(0.01)
        print "STOPPED RUNNING"

    def stop_server(self): 
        self._close_all_clients()
        self.Pump()
        self._running = False
        return

    def Connected(self, client_channel, addr):
        print "New connection:", client_channel        
        self._client_channels.append(client_channel)

        # Let all players know that a new client has connected
        self._notify_all_clients({"action" : "add_new_client", 
                                  "index" : len(self._client_channels)-1})
        
    def _notify_all_clients(self, data, required_keys = None):
        """Send the message in data to all clients"""

        if isinstance(required_keys, basestring):
            # Ensure string is converted to a tuple so we can iterate it correctly
            required_keys = required_keys,
        
        if required_keys is None or all(label in data for label in required_keys):
            for channel in self._client_channels:
                channel.Send(data)        
        else:
            raise ValueError("Missing keys in data: " + required_keys)
        return

    def _close_all_clients(self):
        """Send each client the 'close' command
           then remove the client channel from the stored list of channels"""
        try:
            for i in reversed(range(len(self._client_channels))):
                channel = self._client_channels[i]
                channel.Send({"action" : "close"})                
                del self._client_channels[i]
        except:
            pass

        return

if __name__ == '__main__':
    s = ServerBase()
    s.start_server()