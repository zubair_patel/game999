import server_base

class Server(server_base.ServerBase):
    def __init__(self, host = 'localhost', port = 8000):
        super(self.__class__, self).__init__(host, port)

if __name__ == '__main__':
    s = Server()
    s.start_server()